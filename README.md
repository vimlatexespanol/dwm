# dwm

Esta es mi configuración de dwm. Para cambiar la configuración, modifiquen el archivo 'config.h'.

Para instalar tendrán que ejecutar en terminal lo siguiente:

```
make
sudo make install
```

Si quieren revisar la documentación de dwm, pueden ingresar `man dwm` en la terminal. O leer el archivo `README` el cual es texto plano.

Para poder arrancarlo, bastará con escribir `dwm` en nuestro `~/.xinitrc`. Y en caso de usar un gestor de arranque como `lightdm`, abrá que crear el archivo `dwm.desktop` en la carpeta `/usr/share/xsessions`

```
sudo nano /usr/share/xsessions/dwm.desktop
```

Y agregar lo siguiente:

```
[Desktop Entry]
Encoding=UTF-8
Name=dwm
Comment=Dynamic Window Manager
Exec=dwm
Type=XSession
```
